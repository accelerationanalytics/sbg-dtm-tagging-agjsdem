'use strict';

angular.module('dashboard', []);

//Routers
myApp.config(function($stateProvider) {
	$stateProvider.state('dashboard', {
		url: '/dashboard',
		templateUrl: 'partials/dashboard/dashboard.html',
		data: {
			auth: true
		}
	});

});
