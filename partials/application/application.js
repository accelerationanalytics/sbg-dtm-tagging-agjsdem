'use strict';
angular.module('application', []);

//Routers
myApp.config(function($stateProvider) {

	//Login
	$stateProvider.state('application', {
		url: "/apply/savings-and-investments-products",
		templateUrl: 'partials/application/application-dashboard.html',
		controller: 'applyController'
	});

	$stateProvider.state('saving-application', {
		url: "/apply/{product}",
		templateUrl: 'partials/application/application-savingproducts.html',
		controller: 'applyController'
	});

	$stateProvider.state('saving-application-review', {
		url: "/apply/{product}/review",
		templateUrl: 'partials/application/application-savingsproducts-review.html',
		controller: 'applyController'
	});

	$stateProvider.state('saving-application-confirmation', {
		url: "/apply/{product}/confirmation",
		templateUrl: 'partials/application/application-savingsproducts-confirmation.html',
		controller: 'applyController'
	});

});

myApp.factory('applicationService', function() {
	var savedData = {};
	var applicationData = {
		set: function(item, value){
			savedData[item] = value;
		},
		get: function(item){
			return savedData[item] || 0;
		}
	};
	return applicationData;
});


//Controllers
myApp.controller('applyController', ['$scope', 'applicationService', '$location', '$state', function($scope, applicationService, $location, $state) {
	$scope.location = 'application';
	$scope.productName = $state.params.product;
	$scope.openingBalance = applicationService.get('openingBalance') || '0.00';
	$scope.accountSelection = applicationService.get('accountSelection') || 'NA';
	// console.log($scope);
	$scope.goNext = function() {
		if(typeof $scope.AccountInfo !== "undefined") {
			applicationService.set('openingBalance', $scope.AccountInfo.openbalance.$viewValue);
			applicationService.set('accountSelection', $scope.AccountInfo.accountSelection.$viewValue);
		}
		$location.path("/apply/" + $scope.productName + "/" + arguments[0]);
	}
	$scope.goBack = function() {
		$location.path("/apply/" + $scope.productName);
	}
}]);
